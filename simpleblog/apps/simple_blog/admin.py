from django.contrib import admin
from django.contrib.admin import ModelAdmin

from .models.article import Article
from .models.writer import Writer


class ArticleAdmin(ModelAdmin):
    list_display = ('id', 'title', 'content', 'status', 'written_by',
                    'edited_by', 'created', 'modified')
    readonly_fields = ('created', 'modified')


class WriterAdmin(ModelAdmin):
    list_display = ('id', 'is_editor', 'name', 'created', 'modified')
    readonly_fields = ('created', 'modified',)


admin.site.register(Article, ArticleAdmin)
admin.site.register(Writer, WriterAdmin)
