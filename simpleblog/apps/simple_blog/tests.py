# These were done purely as a proof of knowledge and with a focus on getting
# the app done quickly, otherwise they would have been better organized,
# more numerous and they would have at least used factories
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from simpleblog.apps.simple_blog.enums import ArticleStatus
from simpleblog.apps.simple_blog.models.article import Article


class SimpleBlogTestCase(APITestCase):
    def setUp(self):
        self.django_user = User.objects.create_user(username='Test',
                                                    password='12345')
        self.writer = self.django_user.writer
        self.dummy_article = Article.objects.create(title='Test',
                                                    content='Test',
                                                    written_by=self.writer)
        self.client = APIClient()

    def test_article_create(self):
        url = reverse('simple_blog:article-view')

        # Anon client, should fail
        data = {
            'title': 'Test Article',
            'content': 'Test Content'
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.login(username='Test', password='12345')

        # Create
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['status'], ArticleStatus.PENDING),
        self.assertEqual(response.data['written_by']['id'], self.writer.id),
        self.client.logout()

    def test_article_detail(self):
        url = reverse('simple_blog:article-detail-view',
                      kwargs={'article_id': self.dummy_article.id})

        # Anon client, should fail
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.login(username='Test', password='12345')

        # GET
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Test'),
        self.assertEqual(response.data['content'], 'Test'),
        self.assertEqual(response.data['status'], ArticleStatus.PENDING),
        self.assertEqual(response.data['written_by']['id'], self.writer.id),

        # UPDATE
        data = {
            'title': 'Updated',
            'content': 'Updated',
            'status': ArticleStatus.ACCEPTED,
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], 'Updated'),
        self.assertEqual(response.data['content'], 'Updated'),
        self.assertEqual(response.data['status'], ArticleStatus.PENDING),

        # NOT FOUND
        url = reverse('simple_blog:article-detail-view',
                      kwargs={'article_id': 0})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()

    def test_article_approval(self):
        url = reverse('simple_blog:article-approval-view')

        # Anon client, should fail
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        self.client.login(username='Test', password='12345')

        # User not editor
        self.client.login(username='Test', password='12345')
        data = {
            'id': self.dummy_article.id,
            'status': ArticleStatus.ACCEPTED
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.writer.is_editor = True
        self.writer.save()

        # NOT FOUND
        data = {
            'id': 0,
            'status': ArticleStatus.ACCEPTED
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Can't set to pending
        data = {
            'id': self.dummy_article.id,
            'status': ArticleStatus.PENDING
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Accept
        data = {
            'id': self.dummy_article.id,
            'status': ArticleStatus.ACCEPTED
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['status'], ArticleStatus.ACCEPTED)
        self.assertEqual(response.data['edited_by']['id'], self.writer.id)

        # Not found after accept
        data = {
            'id': self.dummy_article.id,
            'status': ArticleStatus.ACCEPTED
        }
        response = self.client.put(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()
