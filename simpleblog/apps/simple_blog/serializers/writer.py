from rest_framework import serializers

from simpleblog.apps.simple_blog.models.writer import Writer


class WriterDashboardSerializer(serializers.ModelSerializer):
    total_articles = serializers.IntegerField()
    total_articles_last_30 = serializers.IntegerField()

    class Meta:
        model = Writer
        fields = ('id', 'name', 'total_articles', 'total_articles_last_30')


class WriterDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Writer
        fields = ('id', 'name', 'is_editor')
