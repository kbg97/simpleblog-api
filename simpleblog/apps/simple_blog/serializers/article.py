from rest_framework import serializers
from django.utils.translation import gettext as _

from simpleblog.apps.simple_blog.enums import ArticleStatus
from simpleblog.apps.simple_blog.models.article import Article
from simpleblog.apps.simple_blog.serializers.writer import \
    WriterDetailSerializer


class ArticleSerializer(serializers.ModelSerializer):
    written_by = WriterDetailSerializer(read_only=True)

    class Meta:
        model = Article
        fields = ('id', 'title', 'content', 'status', 'created', 'modified',
                  'written_by')
        read_only_fields = ('status',)

    # Made to mock a validation, as the following is done by default
    # if the field is required
    def validate(self, attrs):
        validated_data = super().validate(attrs)

        if not validated_data.get('title', '').strip() \
                or not validated_data.get('content', '').strip():
            raise serializers.ValidationError(
                {'general_errors': _("Title and content can't be empty.")}
            )

        return validated_data

    def create(self, validated_data):
        validated_data['written_by'] = self.context['request'].user.writer
        instance = super().create(validated_data)

        return instance


class ArticleApprovalSerializer(serializers.ModelSerializer):
    written_by = WriterDetailSerializer(read_only=True)
    edited_by = WriterDetailSerializer(read_only=True)

    class Meta:
        model = Article
        fields = ('id', 'title', 'content', 'status', 'created', 'modified',
                  'written_by', 'edited_by')
        read_only_fields = ('title', 'content', 'created', 'modified',
                            'written_by', 'edited_by')

    def validate(self, attrs):
        validated_data = super().validate(attrs)

        if self.instance.status != ArticleStatus.PENDING:
            raise serializers.ValidationError(
                {'general_errors': _("Article is not pending.")}
            )
        elif validated_data.get('status', '') == ArticleStatus.PENDING:
            raise serializers.ValidationError(
                {'general_errors': _("Article can't be moved to pending.")}
            )

        return validated_data

    def update(self, instance, validated_data):
        validated_data['edited_by'] = self.context['request'].user.writer
        instance = super().update(instance, validated_data)

        return instance
