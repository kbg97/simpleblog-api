from django.contrib.auth import get_user_model
from django.db import models
from django_extensions.db.models import TimeStampedModel
from django.db.models.signals import post_save


class Writer(TimeStampedModel):
    is_editor = models.BooleanField(default=False)
    name = models.CharField(max_length=128, default='Needs filling')
    user = models.OneToOneField(get_user_model(), on_delete=models.DO_NOTHING,
                                related_name='writer', unique=True)

    def __str__(self):
        return f"{'Editor' if self.is_editor else 'Writer'} '{self.name}'" \
               f" with id '{self.id}'"


def create_writer_from_user(sender, instance, created, **kwargs):
    if created:
        Writer.objects.create(user=instance)


post_save.connect(create_writer_from_user, sender=get_user_model())
