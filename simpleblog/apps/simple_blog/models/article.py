from django.db import models

from django_extensions.db.models import TimeStampedModel

from simpleblog.apps.simple_blog.enums import ArticleStatus
from simpleblog.apps.simple_blog.models.writer import Writer


class Article(TimeStampedModel):
    title = models.CharField(max_length=128)
    content = models.TextField()
    status = models.CharField(choices=ArticleStatus.choices, max_length=16,
                              default=ArticleStatus.PENDING)
    written_by = models.ForeignKey(Writer, on_delete=models.CASCADE,
                                   related_name='written_articles')
    edited_by = models.ForeignKey(Writer, on_delete=models.CASCADE,
                                  related_name='edited_articles', null=True,
                                  blank=True)

    def __str__(self):
        return f"'{self.title}', by {self.written_by}, id '{self.id}'"
