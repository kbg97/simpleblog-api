from django.urls import path

from simpleblog.apps.simple_blog import views

app_name = 'simple_blog'
urlpatterns = [
    path('', views.DashboardView.as_view(), name='dashboard'),
    path('profile/', views.WriterView.as_view(), name='profile'),
    # used singular because the detail page is also singular in the req.
    # but I was taught long ago to make these plural, don't know who
    # is right at this point
    path('article/', views.ArticleView.as_view(), name='article-view'),
    path('article/<int:article_id>/', views.ArticleDetailView.as_view(),
         name='article-detail-view'),
    path('article-approval/', views.ArticleApprovalView.as_view(),
         name='article-approval-view'),
    path('articles-edited/', views.ArticlesEditedView.as_view(),
         name='articles-edited-view'),
]
