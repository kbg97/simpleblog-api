from django.db.models import TextChoices


class ArticleStatus(TextChoices):
    PENDING = 'PENDING'
    ACCEPTED = 'ACCEPTED'
    REJECTED = 'REJECTED'
