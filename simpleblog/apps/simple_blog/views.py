import datetime

from django.db.models import Count, Q
from django.shortcuts import get_object_or_404
from rest_framework import generics, mixins
from rest_framework.permissions import IsAuthenticated

from simpleblog.apps.simple_blog.enums import ArticleStatus
from simpleblog.apps.simple_blog.models.article import Article
from simpleblog.apps.simple_blog.models.writer import Writer
from simpleblog.apps.simple_blog.permissions import IsEditor
from simpleblog.apps.simple_blog.serializers.article import \
    ArticleSerializer, ArticleApprovalSerializer
from simpleblog.apps.simple_blog.serializers.writer import \
    WriterDashboardSerializer, WriterDetailSerializer
from simpleblog.apps.simple_blog.utils.pagination_classes import \
    DefaultPaginationClass


class DashboardView(generics.GenericAPIView, mixins.ListModelMixin):
    serializer_class = WriterDashboardSerializer
    pagination_class = DefaultPaginationClass

    def get_queryset(self):
        return Writer.objects.prefetch_related('written_articles')\
            .annotate(total_articles=Count('written_articles'),
                      total_articles_last_30=Count('written_articles',
                      filter=Q(written_articles__created__gte=datetime
                               .datetime.today()-datetime.timedelta(days=30))))

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class WriterView(generics.RetrieveAPIView):
    serializer_class = WriterDetailSerializer

    def get_object(self):
        return get_object_or_404(Writer, user=self.request.user)


class ArticleView(generics.CreateAPIView):
    serializer_class = ArticleSerializer


class ArticleDetailView(generics.RetrieveUpdateAPIView):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    # lookup_field = 'id'  # this would have worked but tried
    # to replicate url in req. exactly so param name isn't present on model

    def get_object(self):
        return get_object_or_404(Article, id=self.kwargs['article_id'])


class ArticleApprovalView(generics.ListAPIView, generics.UpdateAPIView):
    permission_classes = [IsAuthenticated, IsEditor]
    pagination_class = DefaultPaginationClass
    queryset = Article.objects.filter(status=ArticleStatus.PENDING)
    serializer_class = ArticleApprovalSerializer

    def get_object(self):
        return get_object_or_404(self.queryset,
                                 id=self.request.data.get('id', None))


class ArticlesEditedView(generics.ListAPIView):
    permission_classes = [IsAuthenticated, IsEditor]
    pagination_class = DefaultPaginationClass
    serializer_class = ArticleSerializer

    def get_queryset(self):
        return Article.objects.filter(edited_by=self.request.user.writer)
