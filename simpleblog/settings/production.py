# flake8: noqa
import dj_database_url

from .common import *

ALLOWED_HOSTS = ['*']

SECRET_KEY = env('SECRET_KEY')

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

BASE_URL = env('BASE_URL')

MIDDLEWARE.insert(1, 'whitenoise.middleware.WhiteNoiseMiddleware')

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
    }
}

db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

# CORS

CORS_ALLOW_ALL_ORIGINS = True  # quick and dirty

# Other

SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
X_FRAME_OPTIONS = 'DENY'
DEBUG = False
